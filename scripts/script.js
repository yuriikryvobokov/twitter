class Twitter {
    constructor({ listElem }) {
        this.tweets = new Posts();
        this.elements = {
            listElem: document.querySelector(listElem)
        }
    }

    renderPosts() {

    }

    showUserPost() {

    }

    showLikesPost() {

    }

    showAllPost() {

    }

    opanModal() {

    }


}

class Posts {
    constructor({ posts = [] } = {}) {
        this.posts = posts;
    }

    addPost(tweet) {
        this.posts.push(new Post(tweet));
    }

    deletePost(id) {

    }

    likePost(id) {

    }
}


class Post {
    constructor(param) {
        this.id = param.id;
        this.userName = param.userName;
        this.nickName = param.nickName;
        this.postDate = param.postDate;
        this.text = param.text;
        this.media = param.media;
        this.likes = param.likes;
        this.liked = false;

    }

    changeLike() {
        this.liked = !this.liked;
        if(this.liked) {
            this.likes++;
        } else {
            this.likes--;
        }
    }
}

new Twitter({
    listElem: '.tweet-list'
})